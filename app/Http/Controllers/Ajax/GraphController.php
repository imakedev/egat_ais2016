<?php

namespace App\Http\Controllers;
use App\Model\MmnameModel;
use App\Http\Requests;
use App\Utils\DBUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Log;

class GraphController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function search()
    {
        Log::info("into initPage->");
        $design_trend_B=Input::get('design_trend_B');
        $search = Input::get('search');
        $sortBy = Input::get('sortBy');
        $orderBy= Input::get('orderBy');

        if(empty($design_trend_B)){
            $design_trend_B=Auth::user()->empId;
        }
        $datas = MmnameModel::on(DBUtils::getDBName())->newQuery();
        if(Input::has('page')){ // paging
            Log::info("into paging");
            $search = session()->get('design_trend_search');
            $sortBy = session()->get('sortBy');
            $orderBy= session()->get('orderBy');
            $design_trend_B= session()->get('design_trend_B');
        }
        if(!empty($search)){
            $datas= $datas->Where(function ($datas) use ($search){
                $datas->orWhere('A', 'LIKE', "%$search%");

            });
        }
        if(!empty($design_trend_B) && $design_trend_B!=-1){
            $datas= $datas->Where('B', '=', "$design_trend_B");
        }
        if(!empty($sortBy) && !empty($orderBy)){
            $datas=$datas->orderBy($sortBy,$orderBy);
        }
        session()->put('sortBy',$sortBy);
        session()->put('orderBy',$orderBy);
        session()->put('design_trend_search',$search);
        session()->put('design_trend_B',$design_trend_B);
        //  $datas=$datas->orderBy('updated_at','DESC')->paginate(10);
        $datas=$datas->paginate(10);
        //$mmtrend_groups = DB::connection(DBUtils::getDBName())->table('mmtrend_group')->where('mmplant','=',session()->get('user_mmplant'))->get();
        $mmtrend_groups = DB::table('mmtrend_group')->where('mmplant','=',session()->get('user_mmplant'))->get();
        return view('ais/graph', ['mmtrendsM'=>$datas,'mmtrend_groups'=>$mmtrend_groups]);
    }
    public function displayGraph()
    {
        Log::info("into displayGraph->");
        //return view('ais/graphRender', ['paramTrendID'=>'3078']);
        return view('ais/trend_setting', ['paramTrendID'=>'3078']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return redirect('ais/addUser');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteSelect(Request $request){

        return redirect('ais/addUser');
    }

    public function destroy($id)
    {

        return redirect('ais/addUser');
    }
}
