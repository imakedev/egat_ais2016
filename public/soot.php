<?php
/**
 * Created by PhpStorm.
 * User: imake
 * Date: 13/07/2016
 * Time: 07:47
 */
set_time_limit(0); //Unlimited max execution time
error_reporting(E_ALL ^ E_NOTICE);
header('Cache-control: private'); // IE 6 FIX
// always modified
header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
// HTTP/1.1
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
// HTTP/1.0
header('Pragma: no-cache');
header('Content-Type: application/json');
echo  getData();
 function getSootFormat($igroup,$index){
    $format_return='';
    // Log::info('into getSootFormat['.$igroup.']');
    switch ($igroup) {
        case 0:
            $format_return=$index.'L';
            break;
        case 1:
            $format_return=$index.'R';
            break;
        case 2:
            $format_return=($index+21).'L';
            break;
        case 3:
            $format_return=($index+21).'R';
            break;
        case 4:
            if($index<8)
                $format_return='B'.$index;
            else if($index<15)
                $format_return='C'.($index-7);
            else if($index<22)
                $format_return='D'.($index-14);
            else if($index<29)
                $format_return='E'.($index-21);
            else
                $format_return='F'.($index-28);
            break;
        case 5:
            if($index<8)
                $format_return='B'.($index+7);
            else if($index<15)
                $format_return='C'.($index);
            else if($index<22)
                $format_return='D'.($index-7);
            else if($index<29)
                $format_return='E'.($index-14);
            else
                $format_return='F'.($index-21);
            break;
        case 6:
            $format_return='AHC1';
            break;
        case 7:
            $format_return='AHC2';
            break;
        case 8:
            $format_return='AHC3';
            break;
        case 9:
            $format_return='AHC4';
            break;
        default:
            $format_return='';
    }
    return $format_return;
}
function getData()
{
    /* */
    $data_back = json_decode(file_get_contents('php://input'));

    $host_db_params = $data_back->{'host_db'};//request('key');
    $user_db_params = $data_back->{'user_db'};//request('formulas');
    $pass_db_param = $data_back->{'pass_db'};//request('startTime');
    $schema_db_param = $data_back->{'schema_db'};//request('endTime');

    $unit_param = $data_back->{'unit'};//request('server')
    $sootDates = $data_back->{'sootDates'};//request('server')

    /*
      $host_db_params = '10.249.91.207';
      $user_db_params = 'Administrator';
      $pass_db_param ='larrabee';
      $schema_db_param ='avg8-13';


      $host_db_params = 'localhost';
      $user_db_params = 'root';
      $pass_db_param ='015482543a6e';
      $schema_db_param ='ais_db';

      //$unit_param ="U08";
      //$sootDates = "2015-11-30";

      $unit_param ="U08";
      $sootDates = "2016-07-13";
      */

/**/

    $conn = @mysql_connect($host_db_params, $user_db_params, $pass_db_param);

    $db = mysql_select_db($schema_db_param);

    if ($conn) {

        // echo "connect success";

    }

    if ($db) {

        //echo "select db success";

    }
    $phase_start_times=['00:00:00','08:00:00','16:00:00'];
    $phase_end_times=['07:59:59','15:59:59','23:59:59'];
    $columns=['D990','D991','D992','D993','D994','D995','D996','D997','D998','D999'];
    $data_phase1=null;
    $data_phase2=null;
    $data_phase3=null;
    $data_phase_list=[$data_phase1,$data_phase2,$data_phase3];
    $data_result_phase1=[];
    $data_result_phase2=[];
    $data_result_phase3=[];
    $data_phase_result_list=[$data_result_phase1,$data_result_phase2,$data_result_phase3];
    $data_blow1=array();
    $data_blow2=array();
    $data_blow3=array();
    $data_blow_list=[$data_blow1,$data_blow2,$data_blow3];
    //$i = 0;
    for($i=0;$i<sizeof($data_phase_list);$i++){
        $sootStartDate_phase=$sootDates." ".$phase_start_times[$i];
        $sootEndDate_phase=$sootDates." ".$phase_end_times[$i];

        $sql = "SELECT EVTIME, D4, D990, D991, D992, D993, D994, D995, D996, D997, D998, D999 ".
            "   from data".strtolower($unit_param) .
            "    WHERE EVTIME BETWEEN '" . $sootStartDate_phase . "' and '" . $sootEndDate_phase . "' ".
            " order by EVTIME asc ";

        $result = mysql_query($sql);
       // $result_key_array = array();
        $data_compare='';
        $max=0;
        $k=0;
        $data_time=null;
        if (!$result) {


             //echo mysql_error();


        } else {


            // echo " query ok..";


        }
        while ($rs = mysql_fetch_array($result)) {
            $new_array_inner = array();
            // $new_array_inner['formula'] = $str;

            $new_array_inner['D4'] =$rs['D4'] ;
            $new_array_inner['EVTIME'] = $rs['EVTIME'];


            $data='';

            $haveData=false;
            $j=0;
            foreach($columns as $column){
                //if($rs->$column!='0'){
                //echo $rs[$column];
                //echo '['.$column.']['.$rs[$column].']<\br>';
                if($rs[$column]!='0'){
                    if($haveData)
                        $data=$data.',';
                   // $sootNumber=$this->getSootFormat($j,$rs->$column);
                    $sootNumber=getSootFormat($j,$rs[$column]);
                    $data=$data.$sootNumber;

                    if(empty($data_blow_list[$i][$sootNumber]))
                        $data_blow_list[$i][$sootNumber]=1;
                    else
                        $data_blow_list[$i][$sootNumber]=$data_blow_list[$i][$sootNumber]+1;;
                    $haveData=true;
                }
                $j++;
            }

            if(empty($data))
                $data='No Soot Operate';
            else
                $data='Soot='.$data;
            //$max=$phase->D4;
            if($k==0){
                $data_time=$rs["EVTIME"];
                $max=$rs["D4"];
                $data_compare=$data;
            }


            if($data_compare!=$data){

                //$phase->data=$data;
                //$phase->time=date_format(new DateTime($phase->EVTIME),'H:i');
                //$phase->amount='Flow='.number_format($phase->D4, 2, '.', ',').' kg/s';

                $new_array_inner['data']=$data_compare;
                $new_array_inner['time']=date_format(new DateTime($data_time),'H:i');
                $new_array_inner['amount']='Flow='.number_format($max, 2, '.', ',').' kg/s';
                // $phase->data=$data_compare;
                //$phase->time=date_format(new DateTime($data_time),'H:i');
                // $phase->amount='Flow='.number_format($max, 2, '.', ',').' kg/s';
                array_push($data_phase_result_list[$i],$new_array_inner);

                // reset
                $data_compare=$data;
                $max=$rs["D4"];
                $data_time=$rs["EVTIME"];
            }

            if($rs["D4"]>$max){
                $max=$rs["D4"];
            }
            if($k==sizeof($data_phase_list[$i])-1){
                //Log::info('lasted   ['.$data_time.'],max ['.$max.']');
                $new_array_inner['data']=$data_compare;
                $new_array_inner['time']=date_format(new DateTime($data_time),'H:i');
                $new_array_inner['amount']='Flow='.number_format($max, 2, '.', ',').' kg/s';

                //$phase->data=$data_compare;
                //$phase->time=date_format(new DateTime($data_time),'H:i');
                //$phase->amount='Flow='.number_format($max, 2, '.', ',').' kg/s';
                array_push($data_phase_result_list[$i],$new_array_inner);
            }
            $k++;
        }

        // Log::info('sootCount_list   ['.sizeof($data_blow_list[$i]).']');
    }

    $data_phase_result_json=json_encode($data_phase_result_list);
    $data_blow_json=json_encode($data_blow_list);
    $json_return="{
        \"data_phase_result_list\":$data_phase_result_json,
        \"data_blow_list\":$data_blow_json
    }";

    return $json_return;
    //return json_encode($data_phase_result_list);
    //return "{data: \"0.406723\",EvTime: \"2014-05-01 14:10:00\"}";
    //return json_decode('{data: "0.406723",EvTime: "2014-05-01 14:10:00"}');
}